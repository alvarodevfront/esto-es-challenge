import { styled } from '@mui/material/styles';
import { TableCell, TableRow, tableCellClasses } from '@mui/material';
import * as Yup from 'yup';

export const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
        backgroundColor: '#FAFA88',
        color: theme.palette.common.black,
    },
    [`&.${tableCellClasses.body}`]: {
        fontSize: 14,
        maxWidth: '150px',
        overflowX: 'hidden',
    },
}));

export const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
        border: 0,
    },
}));

const listHasValues = (list) => list?.length > 0;

const getItemName = (id, list) => {
    const item = list.filter((item) => item.id === id);

    return item[0].name;
};

const descendingComparator = (a, b, orderBy) => {
    if (b[orderBy] < a[orderBy]) {
        return -1;
    }
    if (b[orderBy] > a[orderBy]) {
        return 1;
    }

    return 0;
};

const getComparator = (order, orderBy) => {
    return order === 'desc'
        ? (a, b) => descendingComparator(a, b, orderBy)
        : (a, b) => -descendingComparator(a, b, orderBy);
};

const sortList = (list, page, rowsPerPage, order, orderBy) => {
    const sortedList = list
        .sort(getComparator(order, orderBy))
        .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage);

    return sortedList;
};

export const projectSchema = Yup.object({
    name: Yup.string().required('This field is required.'),
    status: Yup.string().required('This field is required.'),
    description: Yup.string().required('This field is required.'),
    manager: Yup.string().required('This field is required.'),
    assigned: Yup.string().required('This field is required.'),
});

export { sortList, listHasValues, getItemName };
