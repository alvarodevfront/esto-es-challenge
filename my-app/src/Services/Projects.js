import axios from 'axios';
import { successAlert, errorAlert } from './Alerts';

export const PROJECTS_URL = 'http://localhost:4000/projects';

const getProjects = async (id) => {
    let res;

    try {
        if (id) {
            res = await axios.get(`${PROJECTS_URL}/${id}`);
        } else {
            res = await axios.get(PROJECTS_URL);
        }

        return res.data;
    } catch (err) {
        errorAlert(
            'Error',
            err.data || 'An error occurred while getting the projects',
        );
        console.log(err);
    }
};

const editProject = async (id, body) => {
    try {
        const res = await axios.put(`${PROJECTS_URL}/${id}`, body);

        successAlert('Project edited successfully');

        return res.data;
    } catch (err) {
        errorAlert(
            'Error',
            err.data || 'An error occurred while editing the project',
        );

        return err.data || err;
    }
};

const getProjectByKeyword = async (keyword) => {
    try {
        const response = await axios(`${PROJECTS_URL}?Name_like=${keyword}`);

        return response.data;

        //return response;
    } catch (err) {
        return err.response.data;
    }
};

const createProject = async (body) => {
    try {
        const res = await axios.post(PROJECTS_URL, body);

        successAlert('Project created successfully');

        return res.data;
    } catch (err) {
        errorAlert(
            'Error',
            err.data || 'An error occurred while creating the project',
        );

        return err.response.data;
    }
};

const deleteProject = async (id) => {
    try {
        const res = await axios.delete(`${PROJECTS_URL}/${id}`);

        return res.data;
    } catch (err) {
        errorAlert(
            'Error',
            err.data || 'An error occurred while eliminated the project',
        );

        return err.data || err;
    }
};

export {
    getProjects,
    deleteProject,
    editProject,
    getProjectByKeyword,
    createProject,
};
