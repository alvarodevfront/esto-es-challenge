import axios from 'axios';

export const USERS_URL = 'http://localhost:5000/users';

const getUsers = async () => {
    try {
        const res = await axios.get(USERS_URL);

        return res.data;
    } catch (err) {
        // errorAlert('Error', err.data || 'Error al obtener las Actividades');
        console.log(err);
    }
};

export { getUsers };
