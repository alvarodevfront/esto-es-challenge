import { Header } from '../Components';

const Layout = ({ children }) => {
    return <Header>{children}</Header>;
};

export default Layout;
