import React from 'react';
import { Route } from 'react-router-dom';
import Layout from '../Layout/Layout';
import { ProjectCreate, ProjectEdit, MyProjects } from '../Pages/index';

const Routes = () => {
    return (
        <Layout>
            <Route exact component={MyProjects} path="/" />
            <Route component={ProjectCreate} path="/create-project" />
            <Route component={ProjectEdit} path="/edit-project/:id" />
        </Layout>
    );
};

export default Routes;
