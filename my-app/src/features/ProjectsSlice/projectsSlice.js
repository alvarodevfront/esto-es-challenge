import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import {
    getProjects,
    deleteProject,
    getProjectByKeyword,
    createProject,
    editProject,
} from '../../Services/Projects';

export const fetchAllProjects = createAsyncThunk(
    'projects/getProjects',
    async () => {
        const res = await getProjects();

        return res;
    },
);

export const deleteProjectById = createAsyncThunk(
    'projects/deleteProject',
    async (id) => {
        const response = await deleteProject(id);

        return { id, response };
    },
);

export const filterProjectByKeyword = createAsyncThunk(
    'projects/getProjectByKeyword',
    async (keyword) => {
        const response = await getProjectByKeyword(keyword);

        return response;
    },
);

export const createNewProject = createAsyncThunk(
    'projects/createProject',
    async (body) => {
        const response = await createProject(body);

        return response;
    },
);

export const editProjectById = createAsyncThunk(
    'projects/editProject',
    async (id, body) => {
        const response = await editProject(id, body);

        return response;
    },
);

const projectsSlice = createSlice({
    name: 'projects',
    initialState: {
        error: null,
        projects: [],
        loading: false,
    },
    reducers: {},
    extraReducers(builder) {
        builder.addCase(fetchAllProjects.pending, (state, _action) => {
            state.projects = null;
            state.loading = true;
        });
        builder.addCase(fetchAllProjects.fulfilled, (state, action) => {
            state.error = null;
            state.projects = action.payload;
            state.loading = false;
        });
        builder.addCase(fetchAllProjects.rejected, (state, action) => {
            state.error = action.payload;
            state.projects = [];
            state.loading = false;
        });
        builder.addCase(filterProjectByKeyword.pending, (state) => {
            state.projects = null;
            state.loading = true;
        });
        builder.addCase(filterProjectByKeyword.fulfilled, (state, action) => {
            state.error = null;
            state.projects = action.payload;
            state.loading = false;
        });
        builder.addCase(filterProjectByKeyword.rejected, (state, action) => {
            state.error = action.payload;
            state.projects = [];
            state.loading = false;
        });
        builder.addCase(deleteProjectById.pending, (state) => {
            state.loading = true;
        });
        builder.addCase(deleteProjectById.fulfilled, (state, action) => {
            state.error = null;
            state.projects = state.projects.filter(
                (project) => project.id !== action.payload.id,
            );
            state.loading = false;
        });
        builder.addCase(deleteProjectById.rejected, (state, action) => {
            state.error = action.payload;
            state.loading = false;
        });
        builder.addCase(createNewProject.pending, (state) => {
            state.loading = true;
        });
        builder.addCase(createNewProject.fulfilled, (state, action) => {
            state.error = null;
            state.projects = [...state.projects, action.payload];
            state.loading = false;
        });
        builder.addCase(createNewProject.rejected, (state, action) => {
            state.error = action.payload;
            state.loading = false;
        });
        builder.addCase(editProjectById.pending, (state) => {
            state.loading = true;
        });
        builder.addCase(editProjectById.fulfilled, (state, action) => {
            state.error = null;
            state.projects = action.payload;
            state.loading = false;
        });
        builder.addCase(editProjectById.rejected, (state, action) => {
            state.error = action.payload;
            state.loading = false;
        });
    },
});

export default projectsSlice.reducer;

export const selectAllProjects = (state) => state.projects.list;
