import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { getUsers } from '../../Services/Users';

export const fetchAllUsers = createAsyncThunk('users/getUsers', async () => {
    const res = await getUsers();

    return res;
});

const usersSlice = createSlice({
    name: 'users',
    initialState: {
        error: null,
        users: [],
        loading: false,
    },
    reducers: {},
    extraReducers(builder) {
        builder.addCase(fetchAllUsers.pending, (state, _action) => {
            state.users = null;
            state.loading = true;
        });
        builder.addCase(fetchAllUsers.fulfilled, (state, action) => {
            state.error = null;
            state.users = action.payload;
            state.loading = false;
        });
        builder.addCase(fetchAllUsers.rejected, (state, action) => {
            state.error = action.payload;
            state.users = [];
            state.loading = false;
        });
    },
});

export default usersSlice.reducer;

export const selectAllUsers = (state) => state.users.list;
