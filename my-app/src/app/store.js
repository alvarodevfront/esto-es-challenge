import { configureStore } from '@reduxjs/toolkit';
import counterReducer from '../features/counter/counterSlice';
import projectsSlice from '../features/ProjectsSlice/projectsSlice';
import usersSlice from '../features/usersSlices/usersSlice';

export const store = configureStore({
    reducer: {
        counter: counterReducer,
        projects: projectsSlice,
        users: usersSlice,
    },
});
