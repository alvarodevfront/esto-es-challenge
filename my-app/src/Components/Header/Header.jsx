import { useState } from 'react';
import { Link } from 'react-router-dom';
import { AppBar, Box, Toolbar } from '@mui/material';
import Logo from '../../assets/images/logo.png';

const drawerWidth = 240;

const Header = ({ children }) => {
    const [drawerOpen, setDrawerOpen] = useState(false);

    const handleDrawerToggle = () => {
        setDrawerOpen(!drawerOpen);
    };

    return (
        <Box sx={{ display: 'flex', padding: '0' }}>
            <AppBar
                sx={{
                    backgroundColor: 'white',
                    zIndex: { xl: '1201' },
                    height: '60px',
                }}>
                <img
                    src={Logo}
                    style={{
                        width: '60px',
                        height: '60px',
                        marginLeft: '15px',
                        marginTop: '2px',
                    }}
                />
                <Toolbar sx={{ justifyContent: 'space-between' }} />
            </AppBar>
            <Box
                component="main"
                sx={{
                    flexGrow: 1,
                    p: 3,
                    width: { xl: `calc(100% - ${drawerWidth}px)` },
                    padding: '0',
                }}>
                <Toolbar />

                {children}
            </Box>
        </Box>
    );
};

export default Header;
