import React from 'react';
import { useDispatch } from 'react-redux';
import {
    filterProjectByKeyword,
    fetchAllProjects,
} from '../../features/ProjectsSlice/projectsSlice';
import TextField from '@mui/material/TextField';
import style from '../../Styles/ProjectsSearch/projectsSearchForm.module.css';

const ProjectsSearchForm = () => {
    const dispatch = useDispatch();
    const handleChange = (e) => {
        if (e.target.value.length > 2) {
            const searchTerm = e.target.value;

            const keyword =
                searchTerm.charAt(0).toUpperCase() + searchTerm.slice(1);

            dispatch(filterProjectByKeyword(keyword));
        } else {
            dispatch(fetchAllProjects());
        }
    };

    const debounceFn = (fn) => {
        let timeoutId;

        return function () {
            if (timeoutId) {
                clearTimeout(timeoutId);
            }
            const context = this;
            const args = arguments;

            timeoutId = setTimeout(() => {
                fn.apply(context, args);
            }, 500);
        };
    };

    const debouncedHandleChange = debounceFn(handleChange);

    return (
        <div className={style.searchBarContainer}>
            <TextField
                autoComplete="off"
                label="Search your project"
                sx={{ width: '100%' }}
                type="search"
                variant="outlined"
                onChange={debouncedHandleChange}
            />
        </div>
    );
};

export default ProjectsSearchForm;
