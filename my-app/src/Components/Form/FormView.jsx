import React from 'react';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import { Button, Grid, Paper, TextField } from '@mui/material';
import PropTypes from 'prop-types';
import '../../Styles/Form/FormView.css';
const FormView = ({ formik, location, users, handleSetImage }) => {
    return (
        <Paper className="form-container">
            <form noValidate onSubmit={formik.handleSubmit}>
                <Grid>
                    <InputLabel id="Name">Name</InputLabel>
                    <TextField
                        fullWidth
                        error={formik.touched.name && formik.errors.name}
                        helperText={formik.touched.name && formik.errors.name}
                        id="name"
                        labelId="name"
                        name="name"
                        placeholder=""
                        type="text"
                        value={formik.values.name}
                        onChange={formik.handleChange}
                    />
                </Grid>
                <Grid>
                    <InputLabel id="Description">Description</InputLabel>
                    <TextField
                        fullWidth
                        error={
                            formik.touched.description &&
                            formik.errors.description
                        }
                        helperText={
                            formik.touched.name && formik.errors.description
                        }
                        id="description"
                        labelId="description"
                        name="description"
                        placeholder=""
                        type="text"
                        value={formik.values.description}
                        onChange={formik.handleChange}
                    />
                </Grid>
                <Grid>
                    <InputLabel id="Project Manager">
                        Project Manager
                    </InputLabel>
                    <TextField
                        fullWidth
                        select
                        defaultValue={formik.values.manager}
                        error={formik.touched.manager && formik.errors.manager}
                        helperText={
                            formik.touched.manager && formik.errors.manager
                        }
                        id="manager"
                        name="manager"
                        onChange={formik.handleChange('manager')}>
                        {users?.map((user) => {
                            return (
                                <MenuItem
                                    key={user.id}
                                    name="manager"
                                    value={user.name}
                                    onClick={handleSetImage}>
                                    {user.name}
                                </MenuItem>
                            );
                        })}
                    </TextField>
                </Grid>
                <Grid>
                    <InputLabel id="assigned">Assigned to</InputLabel>
                    <TextField
                        fullWidth
                        select
                        defaultValue={formik.values.assigned}
                        error={
                            formik.touched.assigned && formik.errors.assigned
                        }
                        helperText={
                            formik.touched.assigned && formik.errors.assigned
                        }
                        id="assigned"
                        name="assigned"
                        onChange={formik.handleChange('assigned')}>
                        {users?.map((user) => {
                            return (
                                <MenuItem
                                    key={user.id}
                                    image={user.image}
                                    name="assigned"
                                    value={user.name}
                                    onClick={handleSetImage}>
                                    {user.name}
                                </MenuItem>
                            );
                        })}
                    </TextField>
                </Grid>
                <Grid>
                    <InputLabel id="status">Status</InputLabel>
                    <TextField
                        fullWidth
                        select
                        defaultValue={formik.values.status}
                        error={formik.touched.status && formik.errors.status}
                        helperText={
                            formik.touched.status && formik.errors.status
                        }
                        id="status"
                        labelId="status"
                        name="status"
                        onChange={formik.handleChange('status')}>
                        <MenuItem
                            name="status"
                            value={'Enabled'}
                            onClick={handleSetImage}>
                            Enabled
                        </MenuItem>
                        <MenuItem
                            name="status"
                            value={'Disabled'}
                            onClick={handleSetImage}>
                            Disabled
                        </MenuItem>
                    </TextField>
                </Grid>
                <Button
                    sx={{
                        textTransform: 'none',
                        fontSize: '1rem',
                        margin: '10px',
                        background: 'red',
                        textAlign: 'center',
                        lineHeight: '26px',
                        padding: 'auto 3px auto 3px',
                    }}
                    type="submit"
                    variant="contained">
                    {location.pathname === '/create-project'
                        ? 'Create project'
                        : 'Save changes'}
                </Button>
            </form>
        </Paper>
    );
};

FormView.propTypes = {
    formik: PropTypes.object.isRequired,
    loacion: PropTypes.object.isRequired,
    users: PropTypes.array.isRequired,
    handleSetImage: PropTypes.func.isRequired,
};

export default FormView;
