import React, { useEffect, useState } from 'react';
import FormView from './FormView';
import { useParams, useLocation, useHistory } from 'react-router-dom';
import { useFormik } from 'formik';
import { useDispatch, useSelector } from 'react-redux';
import {
    createNewProject,
    editProjectById,
} from '../../features/ProjectsSlice/projectsSlice';
import { fetchAllUsers } from '../../features/usersSlices/usersSlice';
import { v4 as uuidv4 } from 'uuid';
import { editProject } from '../../Services/Projects';
import date from 'date-and-time';
import { projectSchema } from '../../Utils';

const Form = () => {
    const { id } = useParams();
    const location = useLocation();
    const dispatch = useDispatch();
    const { users } = useSelector((state) => state.users);
    const [image, setImage] = useState();
    const [name, setName] = useState();
    const [manager, setManager] = useState();
    const [status, setStatus] = useState();
    const now = new Date();
    const dateValue = date.format(now, 'YYYY/MM/DD HH:mm: A');

    const handleSetImage = (e) => {
        const event = e.target.attributes.name.value;

        if (event === 'manager') {
            setManager(e.target.dataset.value);
        } else if (event === 'status') {
            setStatus(e.target.dataset.value);
        } else if (event === 'assigned') {
            setImage(e.target.attributes.image.value);
            setName(e.target.dataset.value);
        }
    };

    useEffect(() => {
        dispatch(fetchAllUsers());
    }, []);

    const formik = useFormik({
        initialValues: {
            id: id ? location.state.project.id : uuidv4(),
            name: id ? location.state.project.Name : '',
            status: id ? location.state.project.Status : status,
            description: id ? location.state.project.Description : '',
            manager: id ? location.state.project.Manager : manager,
            assigned: id ? location.state.project.Assigned.name : name,
            image: id ? location.state.project.Assigned.image : image,
            date: dateValue,
        },

        validationSchema: projectSchema,

        onSubmit: async (values) => await handleSubmitbecategory(values),
    });

    const handleSubmitbecategory = async (values) => {
        const nameValue = id ? values.assigned : (values.assigned = name);
        const managerValue = id ? values.manager : (values.manager = manager);
        const statusValue = id ? values.status : (values.status = status);

        const body = {
            id: values.id,
            Name: values.name,
            Date: values.date,
            Description: values.description,
            Manager: managerValue,
            Assigned: {
                name: nameValue,
                image: image,
            },
            Status: statusValue,
        };

        console.log(body);

        if (id) {
            editProject(id, body);
        } else {
            dispatch(createNewProject(body));
        }
    };

    return (
        <FormView
            formik={formik}
            handleSetImage={handleSetImage}
            location={location}
            users={users}
        />
    );
};

export default Form;
