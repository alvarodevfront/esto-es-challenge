import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
    Alert,
    Box,
    Container,
    Paper,
    Table,
    TableBody,
    TableContainer,
    TableRow,
    TableCell,
    Avatar,
    TablePagination,
} from '@mui/material';
import {
    fetchAllProjects,
    deleteProjectById,
} from '../../features/ProjectsSlice/projectsSlice';
import { listHasValues } from '../../Utils';
import { sortList } from '../../Utils';
import style from '../../Styles/ProjectsList/ListProjects.module.css';
import { StyledTableCell, StyledTableRow } from '../../Utils';
import LoadSpinner from '../Loader/Loader';
import '../../Styles/Table/TableStyles.css';
import LongMenu from '../LongMenu/LongMenu';
import ProjectsSearchForm from '../Form/ProjectsSearchForm';
import { questionAlert } from '../../Services/Alerts';
// import { questionAlert } from '../../Services/alertsService';

const TableProjects = () => {
    const dispatch = useDispatch();
    const { projects, loading } = useSelector((state) => state.projects);
    const [projectsList, setProjectsList] = useState([]);
    const [order, setOrder] = useState('asc');
    const [orderBy, setOrderBy] = useState('name');
    const [sortedUsersList, setSortedUsersList] = useState([]);
    const [page, setPage] = useState(0);
    const rowsPerPage = 5;

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const emptyRowsToAvoidLayoutJump =
        page > 0 ? Math.max(0, (1 + page) * rowsPerPage - projects.length) : 0;

    const rowHeight = 53;

    const handleDelete = async (id) => {
        const response = await questionAlert(
            `¿Are you sure you want to delete this project?`,
        );

        if (response) {
            dispatch(deleteProjectById(id));
        }
    };

    const updateCategoriesList = () => {
        setProjectsList([...projects]);
    };

    useEffect(() => {
        dispatch(fetchAllProjects());
    }, []);

    useEffect(() => {
        if (!loading) {
            updateCategoriesList();
        }
    }, [loading]);

    useEffect(() => {
        setTimeout(() => {
            const newSortedUsersList = sortList(
                projectsList,
                page,
                rowsPerPage,
                order,
                orderBy,
            );

            setSortedUsersList(newSortedUsersList);
        });
    }, [order, orderBy, page, projectsList]);

    return (
        <div className={style.listContainer}>
            <ProjectsSearchForm />
            <Container sx={{ my: '1rem' }}>
                {!listHasValues(projects) && projects !== null ? (
                    <Alert
                        severity="warning"
                        sx={{
                            margin: '20px auto',
                            justifyContent: 'center',
                            width: 'auto',
                        }}>
                        No results for your search!
                    </Alert>
                ) : null}
                <Box>
                    <Paper>
                        <TableContainer>
                            <Table
                                aria-label="tableTitle"
                                size="small"
                                sx={{ maxWidth: 900 }}>
                                {loading ? (
                                    <TableBody>
                                        <TableRow
                                            style={{
                                                height: rowHeight * 10,
                                            }}>
                                            <TableCell colSpan={3}>
                                                <LoadSpinner />
                                            </TableCell>
                                        </TableRow>
                                    </TableBody>
                                ) : (
                                    <TableBody>
                                        {sortedUsersList?.map((row) => (
                                            <StyledTableRow
                                                key={row.id}
                                                hover
                                                sx={{ height: '3px' }}
                                                tabIndex={-1}>
                                                <StyledTableCell
                                                    align="left"
                                                    component="th"
                                                    scope="row">
                                                    <Box>
                                                        {' '}
                                                        <span
                                                            style={{
                                                                fontSize:
                                                                    '16px',
                                                            }}>
                                                            {row.Name}
                                                        </span>
                                                    </Box>
                                                    <Box>
                                                        <span
                                                            style={{
                                                                fontSize:
                                                                    '12px',
                                                                color: 'gray',
                                                            }}>
                                                            Creation date:{' '}
                                                            {row.Date}
                                                        </span>
                                                    </Box>

                                                    <Box
                                                        sx={{
                                                            display: 'flex',
                                                        }}>
                                                        <Avatar
                                                            alt={
                                                                row.Assigned
                                                                    .image
                                                            }
                                                            src={
                                                                row.Assigned
                                                                    .image
                                                            }
                                                        />
                                                        <span
                                                            style={{
                                                                lineHeight:
                                                                    '40px',
                                                                fontSize:
                                                                    '14px',
                                                                marginLeft:
                                                                    '8px',
                                                            }}>
                                                            {row.Assigned.name}
                                                        </span>
                                                    </Box>
                                                </StyledTableCell>
                                                <StyledTableCell align="right">
                                                    <LongMenu
                                                        handleDelete={
                                                            handleDelete
                                                        }
                                                        project={row}
                                                    />
                                                </StyledTableCell>
                                            </StyledTableRow>
                                        ))}
                                        {emptyRowsToAvoidLayoutJump > 0 && (
                                            <TableRow
                                                style={{
                                                    height:
                                                        rowHeight *
                                                        emptyRowsToAvoidLayoutJump,
                                                }}>
                                                <TableCell colSpan={3} />
                                            </TableRow>
                                        )}
                                    </TableBody>
                                )}
                            </Table>
                        </TableContainer>
                        {!loading && (
                            <TablePagination
                                component="div"
                                count={projects?.length}
                                page={page}
                                rowsPerPage={rowsPerPage}
                                rowsPerPageOptions={[5]}
                                onPageChange={handleChangePage}
                            />
                        )}
                    </Paper>
                </Box>
            </Container>
        </div>
    );
};
//

export default TableProjects;
