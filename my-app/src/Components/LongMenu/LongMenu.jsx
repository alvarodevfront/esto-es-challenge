import * as React from 'react';
import IconButton from '@mui/material/IconButton';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import { ListItemIcon, Typography } from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

const ITEM_HEIGHT = 48;

const LongMenu = ({ project, handleDelete }) => {
    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <>
            <IconButton
                aria-controls={open ? 'long-menu' : undefined}
                aria-expanded={open ? 'true' : undefined}
                aria-haspopup="true"
                aria-label="more"
                id="long-button"
                onClick={handleClick}>
                <MoreVertIcon />
            </IconButton>
            <Menu
                MenuListProps={{
                    'aria-labelledby': 'long-button',
                }}
                PaperProps={{
                    style: {
                        maxHeight: ITEM_HEIGHT * 4.5,
                        width: '20ch',
                    },
                }}
                anchorEl={anchorEl}
                id="long-menu"
                open={open}
                onClose={handleClose}>
                <MenuItem>
                    <ListItemIcon>
                        <DeleteIcon fontSize="small" />
                    </ListItemIcon>
                    <Typography
                        variant="inherit"
                        onClick={() => handleDelete(project.id)}>
                        Delete
                    </Typography>
                </MenuItem>
                <MenuItem>
                    <ListItemIcon>
                        <EditIcon fontSize="small" />
                    </ListItemIcon>
                    <Link
                        style={{
                            textDecoration: 'none',
                            color: 'black',
                        }}
                        to={{
                            pathname: `/edit-project/${project.id}`,
                            state: { project: project },
                        }}
                        variant="inherit">
                        Edit
                    </Link>
                </MenuItem>
            </Menu>
        </>
    );
};

LongMenu.propTypes = {
    project: PropTypes.object.isRequired,
    handleDelete: PropTypes.func,
};

export default LongMenu;
