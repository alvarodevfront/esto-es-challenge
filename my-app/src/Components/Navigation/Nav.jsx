import React from 'react';
import AddIcon from '@mui/icons-material/Add';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import { Box, Button, Toolbar, Typography } from '@mui/material';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

function Nav({ id }) {
    return (
        <Toolbar
            sx={{
                borderBottom: '1px #f7f7f7 solid',
            }}>
            <Box
                sx={{
                    display: 'flex',
                    justifyContent: 'space-between',
                    width: '100%',
                }}>
                {location.pathname === '/' && (
                    <>
                        <Typography
                            mb="auto"
                            mt="auto"
                            sx={{ fontWeight: '550' }}
                            variant="h5">
                            My Projects
                        </Typography>
                        <Button
                            href="/create-project"
                            sx={{
                                textTransform: 'none',
                                fontSize: '1rem',
                                margin: '10px',
                                background: 'red',
                                textAlign: 'center',
                                lineHeight: '26px',
                                padding: 'auto 3px auto 3px',
                            }}
                            variant="contained">
                            <AddIcon />
                            Add project
                        </Button>
                    </>
                )}
                {location.pathname === '/create-project' && (
                    <Box sx={{ display: 'flex' }}>
                        <Link
                            style={{
                                textDecoration: 'none',
                                color: 'black',
                                marginRight: '5px',
                                height: '20px',
                            }}
                            to={{
                                pathname: '/',
                            }}>
                            <ArrowBackIcon
                                sx={{
                                    cursor: 'pointer',
                                    lineHeight: '20px',
                                }}
                            />
                        </Link>
                        <Typography
                            mt={0.2}
                            sx={{
                                fontWeight: '550',
                                marginRight: '15px',
                                color: 'gray',
                                height: '20px',
                                lineHeight: '20px',
                            }}
                            variant="span">
                            Back
                        </Typography>
                        <Typography
                            mb="auto"
                            mt="auto"
                            sx={{ fontWeight: '550' }}
                            variant="h5">
                            Add project
                        </Typography>
                    </Box>
                )}
                {location.pathname === `/edit-project/${id}` && (
                    <Box sx={{ display: 'flex' }}>
                        <Link
                            style={{
                                textDecoration: 'none',
                                color: 'black',
                                marginRight: '5px',
                                height: '20px',
                            }}
                            to={{
                                pathname: '/',
                            }}>
                            <ArrowBackIcon
                                sx={{
                                    cursor: 'pointer',
                                    lineHeight: '20px',
                                }}
                            />
                        </Link>
                        <Typography
                            mt={0.2}
                            sx={{
                                fontWeight: '550',
                                marginRight: '15px',
                                color: 'gray',
                                height: '20px',
                                lineHeight: '20px',
                            }}
                            variant="span">
                            Back
                        </Typography>
                        <Typography
                            mb="auto"
                            mt="auto"
                            sx={{ fontWeight: '550' }}
                            variant="h5">
                            Edit project
                        </Typography>
                    </Box>
                )}
            </Box>
        </Toolbar>
    );
}

Nav.propTypes = {
    id: PropTypes.number.isRequired,
};

export default Nav;
