import React from 'react';
import { Nav } from '../Components';
import Form from '../Components/Form/Form';

const ProjectCreate = () => {
    return (
        <>
            <Nav />
            <Form />
        </>
    );
};

export default ProjectCreate;
