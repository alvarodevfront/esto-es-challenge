import React, { useEffect } from 'react';
import Nav from '../Components/Navigation/Nav';
import { ProjectsList } from '../Components';

const MyProjects = () => {
    return (
        <>
            <Nav />
            <ProjectsList />
        </>
    );
};

export default MyProjects;
