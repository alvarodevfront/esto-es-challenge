import React from 'react';
import Form from '../Components/Form/Form';
import { useParams } from 'react-router-dom';
import { Nav } from '../Components';

const ProjectEdit = () => {
    const { id } = useParams();

    return (
        <>
            <Nav id={id} />
            <Form id={id} />
        </>
    );
};

export default ProjectEdit;
