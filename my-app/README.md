# Esto-es Challenge - Frontend

## Descriptción

Challenge para "Esto-Es". Se trata de una app que permite gestionar los proyectos del usuario. Se podria eliminar y editar cada proyecto o crear uno nuevo.

## Tecnologias usadas 🛠️

### Frontend

- React JS
- react-router
- react-router-dom
- Axios
- formik
- yup
- css
- material-UI
- Redux Toolkit
- JSON server

### Instalación 🔧

- Clona el repositorio: git clone https://AlvaroDevFront@bitbucket.org/alvarodevfront/esto-es-challenge.git
- Entra hasta la carpeta "my-app" en la consola.
- Instala las despendencias: npm install.
- Despues ejecuta "npm run fake-api".
- Abre otra consola, entra nuevamente hasta la carpeta "my-app" y ejecuta "npm run fake-api-users".
- Entra al siguiente link: https://challenge-esto-es-6nbhfvizs-alvarobarreira.vercel.app/ el sitio está deployado pero necesita levantar un "backend" simulado desde el proyecto clonado.

## Rutas

- Mis proyectos: "/"
- Crear un nuevo proyecto: "/create-project"
- Editar un proyecto existente: "/edit-project/:id"

## Author ✒️

- **Alvaro Barreira** - Frontend Developer_ - [AlvaroBarreira](https://github.com/AlvaroBarreira)
